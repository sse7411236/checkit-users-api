﻿namespace CheckItUsersApi.Models
{
    public class UserRole : ModelBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<UserRoles> UserRoles { get; set; }
    }
}
