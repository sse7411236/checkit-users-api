﻿namespace CheckItUsersApi.Models
{
    public class UserRoles : ModelBase
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int UserRoleId { get; set; }
        public UserRole UserRole { get; set; }
    }
}
