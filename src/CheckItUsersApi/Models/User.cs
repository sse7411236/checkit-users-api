﻿namespace CheckItUsersApi.Models
{
    public class User : ModelBase
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public List<UserRoles> UserRoles { get; set; }
    }
}
