using CheckItUsersApi.Dtos;
using CheckItUsersApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace CheckItUsersApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly ILogger<UsersController> _logger;

        public UsersController(ILogger<UsersController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<User>> GetUsers()
        {
            return Ok();
        }

        [HttpGet("{id}")]
        public ActionResult<User> GetUser(int id)
        {
            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult<User> UpdateUser(int id, User user)
        {
            return Ok();
        }

        [HttpPost("register/student")]
        public ActionResult<User> RegisterStudent(RegisterDto registerDto)
        {
            return Ok();
        }

        [HttpPost("register/expert")]
        public ActionResult<User> RegisterExpert(RegisterDto registerDto)
        {
            return Ok();
        }

        [HttpPost("login")]
        public ActionResult Login(LoginDto loginDto)
        {
            return Ok();
        }
    }
}